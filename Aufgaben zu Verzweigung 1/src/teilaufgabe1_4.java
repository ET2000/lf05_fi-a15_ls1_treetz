import java.util.Scanner;

public class teilaufgabe1_4 {

	public static void main(String[] args) {
		
Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein:"); 
		
		int x = scanner.nextInt();
		
		System.out.println("Geben Sie eine zweite Zahl ein:");
		
		int y = scanner.nextInt(); 
		
		// Zahlen größer gleich
		if (x >= y)
		{
			System.out.print("Die Zahl ist größer oder gleich als die zweite Zahl.");
		}
		else 
		{ 
			System.out.print("Die erste Zahl ist nicht größer oder gleich als die zweite Zahl.");
		}
				
		scanner.close();
	}

}

import java.util.Scanner;

public class teilaufgabe2_1 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein:"); 
		
		int x = scanner.nextInt();
		
		System.out.println("Geben Sie eine zweite Zahl ein:");
		
		int y = scanner.nextInt(); 
		
		System.out.println("Geben Sie eine dritte Zahl ein:");
		
		int z = scanner.nextInt(); 
		
		// Wenn die 1.Zahl größer als die 2.Zahl und die 3. Zahl ist, soll eine Meldung ausgegeben werden (If mit && / Und)
		if (x > y && x > z) 
		{
			System.out.print("Die erste Zahl ist größer als die zweite und dritte Zahl.");
		}
				
		scanner.close();

	}

}

import java.util.Scanner;

public class teilaufgabe2_2 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein:"); 
		
		int x = scanner.nextInt();
		
		System.out.println("Geben Sie eine zweite Zahl ein:");
		
		int y = scanner.nextInt(); 
		
		System.out.println("Geben Sie eine dritte Zahl ein:");
		
		int z = scanner.nextInt(); 
		
		// Wenn 3.Zahl größer als 2.Zahl oder 1.Zahl ist...
		if (z > y || z > x)
		{
			System.out.print("Die dritte Zahl ist größer als die erste und zweite Zahl");
		}
				
		scanner.close();


	}

}

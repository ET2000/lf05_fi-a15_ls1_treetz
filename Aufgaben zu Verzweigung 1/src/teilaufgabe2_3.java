import java.util.Scanner;

public class teilaufgabe2_3 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein:"); 
		
		int x = scanner.nextInt();
		
		System.out.println("Geben Sie eine zweite Zahl ein:");
		
		int y = scanner.nextInt(); 
		
		System.out.println("Geben Sie eine dritte Zahl ein:");
		
		int z = scanner.nextInt(); 
		
		// Geben Sie die größte der 3 Zahlen aus. (If-Else mit &&)
		if (x > y && x > z)
		{
			System.out.print("'x' ist die größte Zahl.");	
		}
		else 
		{
			if (y > x && y > z)
			{
				System.out.print("'y' ist die größte Zahl.");
			}
			else 
			{
				System.out.print("'z' ist die größte Zahl.");
			} 
		}
				
		scanner.close();

	}

}

import java.util.Scanner; // Import der Klasse Scanner 

public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
	 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnis = zahl1 + zahl2;  
    
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis );  
    
    
    // Aufgabe 2 weitere Rechenarten
    System.out.print("\nBitte geben Sie eine ganze Zahl ein");
    int zahl3 = myScanner.nextInt();
    
    System.out.print("Bitte geben Sie eine weitere ganze Zahl ein");
    int zahl4 = myScanner.nextInt();
    
    //Multiplikation zahl3 und 4
    int lautet = zahl3 * zahl4;
    
    System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
    System.out.print(zahl3 + "*" + zahl4 + "=" + lautet);
    
    
    //Name und Altersangaben
    System.out.print("\n\nGutentag!");
    System.out.print("\nBitte geben Sie Ihr Name ein: ");
    
    String Name = myScanner.next();
    
    
    System.out.print("\nBitte geben Sie Ihr Alter ein: ");
    
    String Age = myScanner.next();
    

    System.out.print("Ihr Name ist " + Name + " und Sie sind " + Age + " Jahre alt.");
    
    

    myScanner.close(); 
     
  }    
}
import java.util.Scanner;

public class Aufgabe1_Noten {

	public static void main(String[] args) {
		// Noten:  = Sehr gut, 2 = Gut, 3 = Befriedigend,4 = Ausreichend, 5 = Mangelhaft, 6 = Ungenügend
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Zahl zwischen 1 und 6 ein: ");
		int noten = scan.nextInt();
		scan.close();
		switch (noten) {
		case 1: 
			System.out.println("Sehr gut");
			break;
		case 2:
			System.out.println("Gut");
			break;
		case 3:
			System.out.println("Befriedigend");
			break;
		case 4: 
			System.out.println("Ausreichend");
			break;
		case 5:
			System.out.println("Mangelhaft");
			break;
		case 6: 
			System.out.println("Ungenügend");
			break;
		default:
			System.out.print("Ungültiger Wert");
			break;
		}
		
	}

}

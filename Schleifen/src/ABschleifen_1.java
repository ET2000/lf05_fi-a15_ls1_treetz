import java.util.Scanner;
public class ABschleifen_1 {

	public static void main(String[] args) {
		// Aufgaben zu Schleifen 1
		
	Scanner scannr = new Scanner(System.in);
	System.out.println("Bitte geben Sie eine ganze Zahl ein");
	int number = scannr.nextInt();
	scannr.close();
	
	// a) 1,2,3...,n	| for-Schleife
	for (int i = 1; i <= number; i++) {
		System.out.print(i + ", ");
	}
	


	}

}

import java.util.Scanner;

public class aufgabe1_b {

	public static void main(String[] args) {
		// b) n, ..., 3, 2, 1 | for-Schleife

		Scanner scannr = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine ganze Zahl ein");
		int number = scannr.nextInt();
		scannr.close();
		
		for (int i = number; i >= 1; i--) {
			System.out.print(i + ", ");
		}
	}

}

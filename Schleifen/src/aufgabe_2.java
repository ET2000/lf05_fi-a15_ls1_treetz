import java.util.Scanner;

public class aufgabe_2 {

	public static void main(String[] args) {
		// Schleifen AB 1 Aufgabe 2
		for (int i = 1; i <= 200; i++)
		{ 
			if (i % 7 == 0) {
				System.out.println(i + " ist durch 7 teilbar.");
				continue;
			}
			
			if ((i % 5 != 0) && (i % 4 == 0)) {
				System.out.println(i + " ist nicht durch 5 aber durch 4 teilbar.");
			}
		}
	}
}



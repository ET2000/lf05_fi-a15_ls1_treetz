import java.util.Scanner;

public class Aufgabe_1 {

	public static void main(String[] args) {
		// a) 1, 2, 3, ..., n while-Schleife
		Scanner scannr = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine ganze Zahl ein");
		int number = scannr.nextInt();
		scannr.close();
		
		int i = 1;
	while (i < number) {
		System.out.println(i);
		i++;
	}
	}

}
